FROM busybox:glibc

RUN mkdir /data && chmod 0755 /data && chown nobody:nogroup /data
# https://curl.haxx.se/ca/cacert.pem
COPY ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY shokotan /bin

USER nobody
VOLUME /data
WORKDIR /data

ENTRYPOINT ["/bin/shokotan"]

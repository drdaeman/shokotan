package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/boltdb/bolt"
	"github.com/go-openapi/runtime"
	"github.com/mmcdole/gofeed"
	_ "github.com/mmcdole/gofeed/rss"
	"github.com/ogier/pflag"
	tgbot "github.com/olebedev/go-tgbot"
	"github.com/olebedev/go-tgbot/client"
	"github.com/olebedev/go-tgbot/client/attachments"
	"github.com/olebedev/go-tgbot/client/messages"
	"github.com/olebedev/go-tgbot/models"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

const defaultFeedURL = "http://feedblog.ameba.jp/rss/ameblo/nakagawa-shoko/rss20.xml"
const dateFormat = "2006年1月2日"

var feedURL string
var chatID string

var (
	spacingRe = regexp.MustCompile(`[ \r\n\t]+`)
	newlineRe = regexp.MustCompile(`\n\n+`)
)

type traverser struct {
	Output             []postable
	buf                bytes.Buffer
	justEmittedNewline bool
}

func (ctx *traverser) emit(data string) error {
	ctx.justEmittedNewline = strings.HasSuffix(data, "\n")
	_, err := ctx.buf.WriteString(data)
	return err
}

func (ctx *traverser) flush(insert postable) {
	text := strings.TrimSpace(newlineRe.ReplaceAllString(
		strings.Replace(ctx.buf.String(), "\n ", "\n", -1), "\n\n"))
	if text != "" {
		ctx.Output = append(ctx.Output, &textContent{Text: text})
	}
	if insert != nil {
		ctx.Output = append(ctx.Output, insert)
	}
	ctx.buf = bytes.Buffer{}
}

func (ctx *traverser) traverseChild(node *html.Node) error {
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		if err := ctx.traverse(c); err != nil {
			return err
		}
	}
	return nil
}

func getAttr(node *html.Node, attrName string) string {
	for _, attr := range node.Attr {
		if attr.Key == attrName {
			return attr.Val
		}
	}
	return ""
}

func (ctx *traverser) emitWrappedNode(header string, trailer string, node *html.Node) error {
	if err := ctx.emit(header); err != nil {
		return err
	}
	if err := ctx.traverseChild(node); err != nil {
		return err
	}
	return ctx.emit(trailer)
}

func (ctx *traverser) traverse(node *html.Node) error {
	switch node.Type {
	case html.TextNode:
		data := strings.Trim(spacingRe.ReplaceAllString(node.Data, " "), " ")
		return ctx.emit(data)

	case html.ElementNode:
		switch node.DataAtom {
		case atom.Br:
			return ctx.emit("\n")
		case atom.Div:
			prefix := "\n"
			if ctx.justEmittedNewline {
				prefix = ""
			}
			return ctx.emitWrappedNode(prefix, "\n", node)
		case atom.B, atom.Strong:
			return ctx.emitWrappedNode("*", "*", node)
		case atom.A:
			if n := node.FirstChild; n != nil && node.LastChild == n && n.DataAtom == atom.Img {
				// The only child element is an <img> node
				return ctx.traverse(n)
			}
			href := getAttr(node, "href")
			if n := node.FirstChild; n != nil && node.LastChild == n && n.Type == html.TextNode {
				// The only child element is a text node, and it's equal to href
				data := strings.Trim(spacingRe.ReplaceAllString(n.Data, " "), " ")
				if data == href {
					return ctx.emit(fmt.Sprintf("[%s]", href))
				}
			}
			err := ctx.emitWrappedNode("[", "]", node)
			if err != nil {
				return err
			}
			return ctx.emit(fmt.Sprintf("(%s)", href))
		case atom.Img, atom.Iframe:
			src := getAttr(node, "src")
			if src != "" {
				// return ctx.emit(fmt.Sprintf("[%s]", src))
				ctx.flush(&imageContent{URL: src})
				return nil
			}
			return nil
		case atom.Style, atom.Script, atom.Head:
			return nil
		default:
			return ctx.traverseChild(node)
		}
	default:
		return ctx.traverseChild(node)
	}
}

func extractText(htmlData string) ([]postable, error) {
	doc, err := html.Parse(strings.NewReader(htmlData))
	if err != nil {
		return nil, err
	}
	ctx := traverser{Output: make([]postable, 0), buf: bytes.Buffer{}}
	err = ctx.traverse(doc)
	if err != nil {
		return nil, err
	}
	ctx.flush(nil)
	return ctx.Output, nil
}

type postable interface {
	Post(*client.TelegramBot, string) (int64, error)
}

type textContent struct {
	Text string
}

func (c *textContent) Post(bot *client.TelegramBot, title string) (int64, error) {
	var text string
	if title != "" {
		text = fmt.Sprintf("%s\n\n%s", title, c.Text)
	} else {
		text = c.Text
	}
	log.Debug("Posting text")
	result, err := bot.Messages.SendMessage(
		messages.NewSendMessageParams().WithBody(&models.SendMessageBody{
			ChatID:                chatID,
			Text:                  &text,
			ParseMode:             models.ParseModeMarkdown,
			DisableWebPagePreview: true,
		}),
	)
	if err != nil {
		return -1, err
	}
	mid := result.Payload.Result.MessageID
	log.WithField("MessageID", mid).Debug("Posted text")
	return mid, nil
}

type imageContent struct {
	URL string
}

type fetchedImage struct {
	io.Reader
	name string
}

func (i *fetchedImage) Name() string {
	return i.name
}

func (i *fetchedImage) Close() error {
	return nil
}

func fetchImage(url string) (runtime.NamedReadCloser, error) {
	log.WithField("URL", url).Debug("Fetching image")

	res, err := http.Get(url)
	if err != nil {
	    return nil, err
	}
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP error %d", res.StatusCode)
	}

	contentType := res.Header.Get("Content-Type")
	if !strings.HasPrefix(contentType, "image/") {
		return nil, errors.New("Response is not an image")
	}

	blob, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	log.WithField("URL", url).WithField("ContentType", contentType).Info("Fetched image")
	return &fetchedImage{Reader: bytes.NewReader(blob), name: path.Base(url)}, nil
}

func (c *imageContent) Post(bot *client.TelegramBot, title string) (int64, error) {
	img, err := fetchImage(c.URL)
	if err != nil {
		return -1, err
	}
	// defer img.Close()

	log.WithField("URL", c.URL).Debug("Posting photo")
	toSend := attachments.NewSendPhotoParams().WithPhoto(img).WithChatID(chatID)
	if title != "" {
		toSend = toSend.WithCaption(&title)
	}
	result, err := bot.Attachments.SendPhoto(toSend)
	if err != nil {
		return -1, err
	}
	mid := result.Payload.Result.MessageID
	log.WithField("URL", c.URL).WithField("MessageID", mid).Debug("Posted photo")
	return mid, nil
}

type feedItem struct {
	Link    string
	Title   string
	Date    *time.Time
	Content []postable
}

func fetchAll() <-chan feedItem {
	ch := make(chan feedItem)

	go func() {
		defer close(ch)

		fp := gofeed.NewParser()
		feed, err := fp.ParseURL(feedURL)
		if err != nil {
			log.WithError(err).Fatal("Failed fetch RSS feed")
		}

		for i := len(feed.Items) - 1; i >= 0; i-- {
			item := feed.Items[i]
			content, err := extractText(item.Description)
			if err != nil {
				log.WithError(err).Error("Failed to parse entry")
				continue
			}
			log.WithFields(log.Fields{
				"Link":   item.Link,
				"Pieces": len(item.Content),
			}).Debug("Item parsed succesfully")
			ch <- feedItem{
				Link:    item.Link,
				Date:    item.PublishedParsed,
				Title:   item.Title,
				Content: content,
			}
		}
	}()

	return ch
}

func filterSeen(db *bolt.DB, in <-chan feedItem) <-chan feedItem {
	out := make(chan feedItem)
	go func() {
		defer close(out)

		for item := range in {
			seen := false

			if err := db.View(func(tx *bolt.Tx) error {
				b := tx.Bucket([]byte("feedItems"))
				if b == nil {
					log.Info("No bucket `feedItems` found, assuming nothing was seen yet")
					return nil
				}
				if b != nil {
					v := b.Get([]byte(item.Link))
					if v != nil {
						seen = true
					}
				}
				return nil
			}); err != nil {
				log.WithError(err).Fatal("Database read transaction failed")
			}

			if !seen {
				out <- item
			}
		}
	}()
	return out
}

type postedItem struct {
	MessageIDs []int64 `json:"message_ids"`
	Timestamp  string  `json:"timestamp"`
}

func postItem(bot *client.TelegramBot, db *bolt.DB, item *feedItem) error {
	return db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("feedItems"))
		if err != nil {
			return err
		}

		c := len(item.Content)
		mids := make([]int64, c)
		for i, p := range item.Content {
			date := item.Date.Format(dateFormat)
			var title string
			if c > 1 {
				if i > 0 {
					title = fmt.Sprintf("【%d/%d】 %s： %s", i+1, c, date, item.Title)
				} else {
					title = fmt.Sprintf("【%d/%d】 %s： %s\n🌐%s", i+1, c, date, item.Title, item.Link)
				}
			} else {
				title = fmt.Sprintf("%s： %s\n🌐%s", date, item.Title, item.Link)
			}
			mid, err := p.Post(bot, title)
			if err != nil {
				log.WithError(err).Error("Failed to post content")
				mid = -1
			}
			mids = append(mids, mid)
		}

		posted, err := json.Marshal(postedItem{
			MessageIDs: mids,
			Timestamp:  time.Now().Format(time.RFC3339),
		})
		if err != nil {
			log.WithError(err).Error("Failed to encode JSON")
			posted = []byte("null")
		}
		log.WithField("Link", item.Link).Debug("Marking item as posted")
		err = b.Put([]byte(item.Link), posted)
		log.WithField("Link", item.Link).Debug("Marked item as posted")
		return err
	})
}

func main() {
	databaseFile := pflag.String("db", "shokotan.db", "Database file name")
	pflag.StringVar(&feedURL, "feed-url", defaultFeedURL, "RSS feed URL")
	pflag.StringVar(&chatID, "chat-id", "@Shokotan", "Telegram chat ID") // 43025734
	watch := pflag.Bool("watch", false, "Use to watch continuously")
	debug := pflag.Bool("debug", false, "Enable debug logging")
	pflag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	db, err := bolt.Open(*databaseFile, 0640, &bolt.Options{Timeout: 15 * time.Second})
	if err != nil {
		log.WithError(err).Fatal("Failed to open database")
	}
	defer db.Close()

	token := os.Getenv("TELEGRAM_TOKEN")
	if token == "" {
		log.Fatal("No token set. You must define TELEGRAM_TOKEN environment variable")
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	bot := tgbot.NewClient(ctx, token)

	for ok := true; ok; ok = *watch {
		hadPosted := false
		for item := range filterSeen(db, fetchAll()) {
			if hadPosted {
				log.Debug("Sleeping for 10 seconds")
				time.Sleep(10 * time.Second)
			}
			err := postItem(bot, db, &item)
			hadPosted = true
			if err != nil {
				log.WithField("Link", item.Link).WithError(err).Error("Failed to post item")
			} else {
				log.WithField("Link", item.Link).Info("Posted item")
			}
		}
		if *watch {
			log.Info("Sleeping")
			time.Sleep(1 * time.Hour)
		}
	}

	log.Info("Exiting")
}
